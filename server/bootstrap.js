require("ignore-styles");
require("url-loader");
require("file-loader");
// require("babel-register")({
//   ignore: [/(node_modules)/],
//   presets: ["es2015", "react-app"],
//   plugins: ["syntax-dynamic-import", "dynamic-import-node"]
// });
// require("@babel/core").transform("code", {
//   ignore: [/(node_modules)/],
//   plugins: ["@babel/plugin-syntax-dynamic-import"]
// });
require("@babel/polyfill");
require("@babel/register");

require("./server");
