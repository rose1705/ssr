import React from "react";
import { renderToString } from "react-dom/server";
import App from "../src/App";

const path = require("path");
const fs = require("fs");
const filePath = path.resolve(__dirname, "..", "build", "index.html");

export default () => (request, response) => {
  //   response.send("This was rendered on the server in a different file.");
  fs.readFile(filePath, "utf8", function(error, htmlData) {
    if (error) {
      return response.status(404).end();
    }

    const html = renderToString(<App />);

    const mergedData = htmlData.replace(
      '<div id="root"></div>',
      `<div id="root">${html}</div>`
    );

    return response.send(mergedData);
  });
};
